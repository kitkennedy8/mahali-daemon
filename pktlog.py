####################################################################
# Binary Packet Logging Library
#
# author: Ryan Kingsbury
#
# Functions for logging binary packets to a file.  This module stores
# the time stamp  as UTC and makes use of the ISO 8601 string format.
# The binary data itself is encoded as base64.
#
# Format notes:
#  * One packet per line, time and data separated by comma
#  * Line format: <ISO8601 time>,<base64 encoded data>,[framing type]
#    - Framing type field is optional
#    - Lines w/o framing type specifies are assumed to be normal
#      byte-stuffed format:
#    [ SOP ][ ID ][  LENGTH  ][DUP.LENGTH][   CRC    ][PAYLOAD...
# byte: 0    1       2,3         4,5         6,7         8-N
#
#
####################################################################
from datetime import datetime
import base64

LINE_SEP = "\n"

def make_entry_from_string(time,pktraw):
    """ Make log entry line.

    pktraw is a string of bytes that have been deframed.
    """
    # Convert the time string
    time_str = datetime_to_iso8601(time)

    enc_data = base64.b64encode(pktraw)

    return time_str + ',' + enc_data + LINE_SEP

def make_entry_from_pkt(time,pkt):
    """ Make log entry line.

    pkt object is normally contains the fully constructed packet, just
    as it would be passed off to the framer
    """
    # Convert the time string
    time_str = datetime_to_iso8601(time)

    # Get the framed data
    buffer = pkt['framer_output_bytes']
    enc_data = base64.b64encode(buffer)

    # Get the packet framing scheme
    pktframing = pkt.get('framing',"OBC").upper()

    return time_str + ',' + enc_data + ','+ pktframing + LINE_SEP

def read_entry(s):
    """ Parse entry back into datetime object and binary string of
    bytes.
    """

    time_str = s.split(',')[0]
    time = iso8601_to_datetime(time_str)
    
    data = base64.b64decode(s.split(',')[1])

    try:
        framingType = s.split(',')[2].strip()
    except IndexError:
        framingType = "OBC"
    
    return (time,data,framingType)

def iso8601_to_datetime(iso_string):
    """ Converts an ISO8601 string back into a Python datetime object. """

    return datetime.strptime( iso_string, "%Y-%m-%dT%H:%M:%S.%f" )

def datetime_to_iso8601(dt):
    """ Converts a Python datetime object into an ISO8601 string.

    Note: you might think it would be a good idea to use Python's
    built-in ISO8601 formatting function...turns out that's a bad
    idea.  if the time microsecond field is zero, it drops the
    decimal point and trailing zeros in the seconds field.  this
    breaks stuff downstream (like gen_matlab.py).
    """
    return dt.strftime("%Y-%m-%dT%H:%M:%S.%f")
