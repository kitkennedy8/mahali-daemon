####################################################################
# Dashboard
#
# author: Ryan Kingsbury
#
# This application provides a web-based command and telemetry
# interface to an embedded system.
#  * raw logging of binary data from hardware
#  * Web-based interface for display of TLM and generation of CMDs
#
# MAHALI: This has been hacked into a Mahali "daemon" to handle
# science data collection and to provide a web interface for the
# app
#
# For command line help:
#     python dashboard.py --help
#
####################################################################

# External libraires
import sys
import Queue
import os
import json
from datetime import datetime
from twisted.internet.protocol import Protocol, ClientFactory, ReconnectingClientFactory
from twisted.internet.serialport import SerialPort
from twisted.internet import reactor

# Custom libraries/modules
from daemon import Daemon
from rotfile import RotatingFile
import pktlog
import deframer
import pktparser
import framer
import pktbuilder

# Change working directory to that of the this script
# more info here: http://stackoverflow.com/questions/1432924/python-change-the-scripts-working-directory-to-the-scripts-own-directory
os.chdir(os.path.dirname(sys.argv[0]))

CONFIG_FILE="config.json"
CONFIG = json.load(open(CONFIG_FILE,'r'))
CONFIG_STR = json.dumps(CONFIG,sort_keys=True,
                        indent=4, separators=(',', ': '))
CONFIG['start_time'] = datetime.utcnow()
CONFIG['start_time_string'] = CONFIG['start_time'].strftime("%Y%m%d_%H%M%S") # time string
print CONFIG_STR

# Global data structures
HISTORY_LENGTH = 1000 # number of TLM/CMD packets to hold in memory

# Output log file naming
LOG_PATH = "logs"
LOG_BASE_NAME = datetime.utcnow().strftime("%Y%m%d_%H%M%S") # time string

class HardwareProtocol(Protocol):
    """ Implements our custom framing protocol through use of the
    deframer and framer modules. """

    # Recent telemetry database
    db = {}

    # Data reception objects
    raw_pkt_queue = Queue.Queue()
#    pkt_deframer = deframer.BinaryPacketReceiver(raw_pkt_queue)
    
    # making framer a class var so it can be overridden in child classes 
#    pkt_parser = pktparser.Parser()
    
    # Data transmission objects
    pkt_builder = pktbuilder.Builder()
    # pkt_framer is made after the connection is established (see makeConnection() below)
    
 #   cmd_logfile = open(os.path.join(LOG_PATH,LOG_BASE_NAME+".cmd.log"),'w')

    science_logfilename = os.path.join(CONFIG['LOG_PATH'],
                                       'science',
                                       CONFIG['SITE_NAME']+'_')
    tlm_logfile = RotatingFile(path=science_logfilename,
                               starttime=CONFIG['start_time'])
    
    def transmitPacket(self, pkt):
        pass
    
        # # Build the packet
        # pktbuilt = self.pkt_builder.build_packet(pkt)

        # # Send the packet
        # pktsent = self.pkt_framer.xmit_packet(pktbuilt)
        # txtime = datetime.utcnow()

        # # Log the raw packet to a file
        # # Note: file I/O could technically block so we really
        # # shouldn't be doing this here as it violates the main
        # # tenet of async. programming.  That said, file I/O
        # # should be fast so we'll ignore this matter for now.
        # # TODO: use twisted's non-blocking logging infrastructure
        # entry = pktlog.make_entry_from_pkt(txtime,pktsent)
        # self.cmd_logfile.write( entry )
        
    def makeConnection(self, transport):
        
        # Establish the connection (either serial or TCP)
        Protocol.makeConnection(self,transport)
        
        # Once the connection has been made, attach it to the flask app
        flaskapp = transport.protocol.factory.flaskapp
        setattr(flaskapp,"tlm_db",transport.protocol.db)
        setattr(flaskapp,"transport",transport)
        setattr(flaskapp,"APPCONFIG",CONFIG)
        
        linkmode = transport.protocol.factory.linkmode
        
        # Create a framer object
        if linkmode is "serial":
            self.pkt_framer = framer.SerialFramer(transport)
        elif linkmode is "cadet":
            self.pkt_framer = framer.CadetFramer(transport)
        else:
            raise "link mode is invalid"

            
    # def update_tlm_db(self, pkt):
    #     """ Updates the telemetry database structure."""
        
    #     # Make sure database has the necessary key (packet name), if
    #     # not create one.
    #     if pkt['name'] not in self.db:
    #         self.db[pkt['name']] = []

    #     # Append the new packet to the database, keeping only the most
    #     # recent N packets
    #     self.db[pkt['name']] = self.db[pkt['name']][-(HISTORY_LENGTH-1):]+[pkt]

    #     print pkt['name'], json.dumps(pkt['values'], encoding="latin-1")
        
    #     return
        
    def dataReceived(self, data):
        """ This function is called whenever the transport layer
        receives bytes."""

        print "Received %i bytes"%len(data)
        self.tlm_logfile.write(data)
        return

    def sendPacket(self):
        print "sendPacket() stub"
        return
        
    def connectionLost(self, reason):
        # Close the logfile
        print "Closing logfiles:"
        print " Telemetry: ",os.path.join(LOG_PATH,LOG_BASE_NAME+".tlm.log")
#        print " Commands:  ",os.path.join(LOG_PATH,LOG_BASE_NAME+".cmd.log")
        self.tlm_logfile.close()
#        self.cmd_logfile.close()
        
class HardwareProtocolFactory(ClientFactory):
    protocol = HardwareProtocol
    
    def __init__(self,flaskapp,linkmode=None):
        """The flask app needs to be provided to the factory so that
        it can be attached to newly-created protocols.
        """
        self.flaskapp = flaskapp

        # Validate, then store link mode
        if linkmode not in ['serial','cadet']:
            raise "Invalid link mode"
            
        self.linkmode = linkmode

        
def setup_flask_web_interface():
    """ Instantiates the Flask web framework."""
        
    # Import the web interface app
    from webif import app

    # run in under twisted through wsgi
    from twisted.web.wsgi import WSGIResource
    from twisted.web.server import Site
    resource = WSGIResource(reactor, reactor.getThreadPool(), app)
    site = Site(resource)
    reactor.listenTCP(8080,site)

    return app

class MahaliDaemon(Daemon):
    def run(self):
        flaskapp = setup_flask_web_interface()

        # Establish connection to local hardware_sim.py server
        factory = HardwareProtocolFactory(flaskapp,linkmode="serial")
        #conn = reactor.connectTCP("localhost",10000,factory)

        hwProtocol = factory.buildProtocol(None) 
        # Next, bind the protocol to a the serial port
        port = SerialPort(hwProtocol,
                          CONFIG['SERIAL_PORT'],
                          reactor,
                          baudrate=int(CONFIG['SERIAL_BAUDRATE']))

        print "Loaded config.json:"
        print CONFIG_STR
        
        reactor.run()
 
    

if __name__ == '__main__':
    ADMIN_LOGFILE = CONFIG['SITE_NAME']+'_'+CONFIG['start_time_string']+'.log'
    ADMIN_LOGFILE_FULLPATH = os.path.join(CONFIG['LOG_PATH'],
                                          './admin',
                                          ADMIN_LOGFILE)
    daemon = MahaliDaemon('/tmp/mahali-daemon.pid',stdout=ADMIN_LOGFILE_FULLPATH,
                          stderr='/tmp/stderr')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
    



