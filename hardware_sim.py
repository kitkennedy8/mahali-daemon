######################################################################
# Simple hardware emulator
#
# This script waits for a client (normally Dashboard) to connect to
# the specified TCP socket.  Once the connection is established, this
# script periodically generates sample telemetry packet(s).  The
# script also echos any commands that it receives.
#
######################################################################
import optparse, os
import json
import random
from twisted.internet.protocol import ServerFactory, Protocol

import pktbuilder
import framer

def parse_args():
    usage = """usage: %prog [options] 
       port : TCP port to serve packets
    """

    parser = optparse.OptionParser(usage)

    help = "The port to listen on. Default to port 10000."
    parser.add_option('--port', type='int', help=help, default=10000)

    help = "The interface to listen on. Default is localhost."
    parser.add_option('--iface', help=help, default='localhost')

    options, args = parser.parse_args()

    return options

def fill_packet_random(pkt_in):
    """ Fills a packet with random data. """
    import random
    pkt = pkt_in.copy()
    pkt['values'] = {}

    def get_rand_val(field):
        t = field['type'].lower()        
        # If it is an int, pick a random number between 0-100
        if 'int' in t:
            return random.randint(0,100)
        # If it is a float, pick between (-1,1)
        elif t=='float' or t=='double':
            return random.uniform(-1,1)
        else:
            return 0

    for f in pkt['fields']:
        if 'array' not in f:
            pkt['values'][f['name']] = get_rand_val(f)
        else:
            vals = []
            for i in range(int(f['array'])):
                vals.append(get_rand_val(f))
            pkt['values'][f['name']] = vals

    return pkt
    
class HwTestProtocol(Protocol):

    def __init__(self):
        self.pktbuilder = pktbuilder.Builder()

        # Load up a test packets
        pkt = json.load(open("./packet_defs/tlm_example.json",'r'))
        self.test_pkt = pkt
        
        self.connAlive = True

    def sendAgain(self):
        print "transmitting"

        # Fill packet with random data
        self.test_pkt = fill_packet_random(self.test_pkt)
        
        # Build packet
        pktbuilt = self.pktbuilder.build_packet(self.test_pkt)

        # Frame the packet
        pframed = self.pktframer.xmit_packet(pktbuilt)

        # Do it again in a little while, if the connection is still open
        if self.connAlive:
            from twisted.internet import reactor
            reactor.callLater(0.2,self.sendAgain)

    def dataReceived(self,data):
        """ Echo all received data"""
        self.transport.write(data)
            
    def connectionMade(self):
        self.pktframer = framer.SerialFramer(self.transport)
        
        self.sendAgain()

    def connectionLost(self, reason):
        # Signal the infinite sendAgain loop to terminate
        print "connection closed"
        self.connAlive = False


class HwTestFactory(ServerFactory):

    protocol = HwTestProtocol


def main():
    options = parse_args()

    factory = HwTestFactory()

    from twisted.internet import reactor

    port = reactor.listenTCP(options.port or 0, factory,
                             interface=options.iface)

    print 'Serving packets on %s.' % (port.getHost(),)

    reactor.run()


if __name__ == '__main__':
    main()
