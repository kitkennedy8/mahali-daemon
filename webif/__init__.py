from flask import Flask

class MyFlask(Flask):
    """ Customize Jinja2 options, per the recommendation here:

    http://flask.pocoo.org/mailinglist/archive/2011/9/25/loading-a-jinja2-extension-in-to-flask/#c1de919e35c032984f0d2dfa50f6bf7e
    """
    jinja_options = dict(Flask.jinja_options)
    jinja_options.setdefault('extensions',[]).append('jinja2.ext.loopcontrols')

app = MyFlask(__name__)
app.debug = True
app.secret_key = '\xb6T\xfb\xdd3\x9b\x0c\xb0\xb9\xe6\x19\n\xc41\xb3$D\xf4\xd7\x04\xd9\xb9\xa1\xc0'

from flask.ext.bootstrap import Bootstrap
Bootstrap(app)
# Disable jquery load from CDN
app.config["BOOTSTRAP_JQUERY_VERSION"]=None


import webif.views
import webif.views_plotting

