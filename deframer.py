####################################################################
# Binary Packet Deframer
#
# author: Ryan Kingsbury
#
# This module is used to deframe binary-encoded packets from the
# embedded system.  The deframing process includes the following
# steps: 
#   1) Identification of the start-of-packet sequence
#   2) Validation of packet SOP character and ID field
#   3) Validation of redundant packet length fields
#   4) Validation of CRC
#   5) "Destuffing" of escaped characters
#
#
# Long packet format overview:
#     [SOP][ ID ][  LENGTH  ][DUP.LENGTH][   CRC    ][PAYLOAD...
# byte: 0    1       2,3         4,5         6,7         8-N
#
# The [SOP] field is always the escape flag, 0x30.  This value is
# removed from the body of the packet as necessary via performing
# byte-stuffing.
#
# The [ ID ] field acts as a unique identifyer for each packet type.
#
# The duplicated [LENGTH] fields give the number of bytes in the
# payload (i.e. the number of bytes following the CRC).
#
# [CRC] is a 16-bit CRC, see source code for generation details
#
# Short packet format overview:  (rarely used)
#     [SOP][ ID ]
# byte: 0    1    
#
####################################################################

import sys
import os
import time
import serial
import logging
import Queue
import glob
import json
import crc16pure as crc16
import struct

DEBUG=False
VERBOSE=False

# Packet escape flag.  This marks the SOP (start of packet)
DEFAULT_ESCAPE_FLAG = 0x30

MAX_PACKET_LENGTH = 4096

class BinaryPacketReceiver:
    def __init__(self,out_queue,schema_path="packet_defs",drop_bad_CRC=True):
        """ Initialiation function for object.  An output queue, of
    type Queue.Queue() must be specified.  Optionally, the path for
    packet definitions ("schema") can be provided."""

        self.stats = {"ERR_CRC_BAD":0,
                      "ERR_INVALID_ID":0,
                      "ERR_PKTID_WAS_ESC":0,    
                      "ERR_RUNT_PACKET":0,   # long packet but smaller than min packet size (8)
                      "ERR_CROPPED_PACKET":0,# long packet but length fields disagree with received packet size
                      "ERR_LENGTH_MISMATCH":0,# Dupe length fields disagree
                      "ERR_OVERSIZED_PACKET":0, # packet size greater than maximum allowable
                      "IGNORED_BYTE":0,
                      "SOP_COUNT_SHORT":0,
                      "SOP_COUNT_LONG":0,
                      "PKT_COUNT_LONG":0,
                      "PKT_COUNT_LONG_FROM_BUS":0, # Counts long packets with TYPE in range 0x80-0xFF
                      "PKT_COUNT_LONG_FROM_PAYLOAD":0, # Counts long packets with TYPE in range 0x00-0x7F
                      "FLAG_COUNT":0,
                      "BYTES_RECEIVED":0, # Counts all bytes received on line (including stuffing, packet fragments, noise, etc)
                  }

        self._ESCAPE_FLAG = DEFAULT_ESCAPE_FLAG       # Marks SOP
        self._drop_bad_CRC = drop_bad_CRC
        
        # Create lists to hold valid packet IDs
        self._PKT_ID_SHORT = []
        self._PKT_ID_LONG = []
        self._PKT_ID_GS = []
        
        # Load schema in order to populate valid packet ID lists
        for fname in glob.iglob(os.path.join(schema_path,'*.json')):
            try: 
                self.load_schema(fname)
            except:
                print "\n\n*****************************************************"
                print "Error parsing packet definition file: %s"%fname
                print "*****************************************************"
                raise

        self.pkt_queue = out_queue # Output queue for packets

        self.state = "SOPa" # Initial state of receiver FSM
        self._rx_buffer = ''

    def load_schema(self, fname):
        """ Loads schema from specified filename and adds it to the
        schema dictionary. """

        with open(fname,'r') as f:
            if DEBUG: print "loading", fname
            j = json.load(f)
            if j['format'].upper()=='LONG':
                self._PKT_ID_LONG.append(j['id'])
            elif j['format'].upper()=='SHORT':
                self._PKT_ID_SHORT.append(j['id'])
            elif j['format'].upper()=='GROUND_STATION':
                self._PKT_ID_GS.append(j['id']) 
            else:
                sys.stderr.write('ERROR: Unknown packet format in %s.  Should be either "short" or "long".\n'%fname)

        return

    def recv_byte(self,i):
        """ Process received byte i. """
        if DEBUG:
            #h = '['+' '.join('0x%02x' % b for b in self._rx_buffer)+']'
            #print h
            print "recv_byte(0x%X), buflen=%i, state=%s,"%(i,len(self._rx_buffer),self.state),

        # Increment bytes received statistic
        self.stats['BYTES_RECEIVED']+=1
            
        # Receive state machine
        if self.state=="SOPa":
            # If it is a flag, then initialize packet buffer and set
            # next state to look for non-flag character (the SOP cond.)
            if i==self._ESCAPE_FLAG:
                self._rx_buffer = chr(self._ESCAPE_FLAG)
                self.state="SOPb"
                if DEBUG: print "found escape"
                return
            # If it isn't then just ignore it
            if DEBUG: print "ignoring char"
            self.stats['IGNORED_BYTE']+=1
            return
                
        elif self.state=="SOPb":
            if i==self._ESCAPE_FLAG:
                # Per the ICD, the packet ID field must not be the
                # escape flag.
                # If it is, just reset the state machine
                self.state="SOPa"
                if DEBUG: print "invalid PKY TYPE field"
                self.stats["ERR_PKTID_WAS_ESC"]+=1
                return

            if i!=self._ESCAPE_FLAG:
                if DEBUG: print "found SOP condition",
                
                # This character just received is the packet ID, this
                # determines if we have a long or short packet type.

                # If this is a short packet ID, we are done receiving it.
                if i in self._PKT_ID_SHORT:
                    # Add the byte to the RX buffer
                    self._rx_buffer += chr(i)
                    # Move the (two-byte) packet to the packet queue
                    self.pkt_queue.put(self._rx_buffer)

                    h = '['+' '.join('0x%02x' % ord(b) for b in self._rx_buffer)+']'
                    if VERBOSE: print "Short Packet:", h

                    # Next time, start looking for a SOP again
                    if DEBUG: print "short packet ID found (%X)"%i
                    self.state="SOPa"
                    self.stats['SOP_COUNT_SHORT']+=1
                    return

                # If this is a long packet ID, we have more work to do
                elif i in self._PKT_ID_LONG:
                    # Add the byte to the RX buffer
                    self._rx_buffer += chr(i)

                    # Next time, we start receiving the rest of the
                    # packet (lengths, CRC, payload)
                    self.state="RX_LONG"

                    # Initialize the escape found flag
                    self._esc_found=False
                    if DEBUG: print "long packet ID found (%X)"%i
                    self.stats['SOP_COUNT_LONG']+=1
                    return

                # If the packet ID isn't recognized, reset the state machine
                else:
                    self.state="SOPa"
                    if DEBUG: print "unknown packet ID"
                    self.stats['ERR_INVALID_ID']+=1
                    return


        elif self.state == "RX_LONG":
            
            if i==self._ESCAPE_FLAG:

                if self._esc_found:
                    if DEBUG: print "escape found (2nd flag in a row)",
                    # This is an escaped character, add it once to the
                    # buffer (below) and clear the escape found flag.
                    self._esc_found=False
                else:
                    # This is an escape character, set the found flag
                    self._esc_found=True
                    if DEBUG: print "flag found"
                    self.stats['FLAG_COUNT']+=1
                    return

            if not self._esc_found:
                # This is a regular character
                if DEBUG: print "regular character"
                self._rx_buffer += chr(i)

            else:
                # We have found a SOP condition unexpectedly
                if len(self._rx_buffer)<8:
                    if DEBUG: print "Unexpected SOP, runt packet?"
                    self.state="SOPa"
                    self.stats['ERR_RUNT_PACKET']+=1
                    return
                else:
                    if DEBUG: print "Unexpected SOP"
                    self.state="SOPa"
                    self.stats['ERR_CROPPED_PACKET']+=1

                    if VERBOSE:
                        h = '['+' '.join('0x%02x' % ord(b) for b in self._rx_buffer)+']'
                        print "CROPPED: type=%i rx=%X lenbuf=%i buf="%(ord(self._rx_buffer[1]),i,len(self._rx_buffer)), h 

                    return

            # If we make it this far, we have just added a new byte to
            # the rx buffer.  If possible we need to check redundant
            # length fields for match.  Later, we check the CRC if the
            # entirety of the packet has been received.

            # See if we have received the first six bytes of the packet
            if len(self._rx_buffer)==6:
                # if so, check to make sure the length fields match
                if not ( (self._rx_buffer[2]==self._rx_buffer[4]) and
                         (self._rx_buffer[3]==self._rx_buffer[5]) ):
                    self.state="SOPa"
                    if DEBUG: print "length field mismatch!"
                    self.stats['ERR_LENGTH_MISMATCH']+=1
                    return
                else:
                    # If they do match, we can compute the expected
                    # packet length
                    self._current_pkt_length = ord(self._rx_buffer[2])+(ord(self._rx_buffer[3])<<8)+8

                    # Make sure the packet length isn't above the maximum
                    if self._current_pkt_length > MAX_PACKET_LENGTH:
                        if DEBUG: print "oversized packet found, length is %i"%self._current_pkt_length

                        self.stats['ERR_OVERSIZED_PACKET']+=1
                        self.state="SOPa"
                        return

                    if DEBUG: print "expected packet length is %i"%self._current_pkt_length
                    return
            
            # If we have 8 or more bytes, we may have received the
            # entire packet...check on this
            if len(self._rx_buffer)>=8:

                # Determine if we have received the entire packet
                if len(self._rx_buffer)==self._current_pkt_length:
                    # If it does, then we have received the entire packet

                    # Compute CRC
                    # First, extract CRC from packet
                    crc_pac = struct.unpack_from("H",self._rx_buffer,6)[0]
                    
                    # Extract data
                    rxcpy = self._rx_buffer[8:]
                  
                    # Create a temp copy of rx buffer to add zeroes if odd
                    if len(self._rx_buffer) % 2 != 0:
                        rxcpy = rxcpy + chr(0)
                    
                    # Calculate CRC across packet
                    crc_calc = crc16.crc16xmodem(rxcpy)
                
                    # If the CRC fails, update stats and drop the packet (if requested)
                    if crc_pac != crc_calc:
                        if DEBUG:
                            print "CRC does not match"
                        self.stats["ERR_CRC_BAD"]+=1
                        if self._drop_bad_CRC:
                            self.state="SOPa"
                            return
                            
                    # Append the packet to the output queue
                    self.pkt_queue.put(self._rx_buffer)

                    h = '['+' '.join('0x%02x' % ord(b) for b in self._rx_buffer)+']'
                    if VERBOSE: print "Long Packet:", h

                    # Update reception statistics
                    self.stats['PKT_COUNT_LONG']+=1
                    if  0x00 <= ord(self._rx_buffer[1]) <= 0x7F:
                        self.stats['PKT_COUNT_LONG_FROM_PAYLOAD']+=1
                    if  0x80 <= ord(self._rx_buffer[1]) <= 0xff:
                        self.stats['PKT_COUNT_LONG_FROM_BUS']+=1
                    
                    # Reset the receiver state machine
                    self.state="SOPa"


if __name__ == '__main__':
    import optparse

    parser = optparse.OptionParser(
        usage = "%prog [options] port",
        description = "Telemetry receiver script for MicroMAS")

    parser.add_option("-v", "--verbose",
        dest = "verbosity",
        action = "count",
        help = "print more diagnostic messages (option can be given multiple times)",
        default = 0
    )

    (options, args) = parser.parse_args()

    if len(args) != 1:
        parser.error('serial port name required as argument')

    if options.verbosity > 3:
        options.verbosity = 3
    level = (
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
        logging.NOTSET,
        )[options.verbosity]
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('root').setLevel(logging.INFO)
    logging.getLogger('rfc2217').setLevel(level)

    # connect to serial port
    ser = serial.Serial()
    ser.port     = args[0]
    ser.timeout  = 1     # required so that the reader thread can exit
    ser.baudrate = 115200

    logging.info("Telemetry receiver - type Ctrl-C / BREAK to quit")

    try:
        ser.open()
    except serial.SerialException, e:
        logging.error("Could not open serial port %s: %s" % (ser.portstr, e))
        sys.exit(1)

    logging.info("Opened serial port: %s" % (ser.portstr,))

    # Create BinaryPacketReceiver object
    rx = BinaryPacketReceiver()

    while True:

        try:
            rx_data = ser.read(1)
            if len(rx_data)>0:
                rx.recv_byte(ord(rx_data))

            # for b in rx_data:
            #     #print "%02X "%ord(b), 
            #     rx.recv_byte(ord(b))

            #print len(rx.pkt_queue),rx.state
        except KeyboardInterrupt:
            print rx.stats
            break
        # except socket.error, msg:
        #     logging.error('%s' % (msg,))

    logging.info('--- exit ---')
