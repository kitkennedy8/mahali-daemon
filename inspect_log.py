####################################################################
# Load telemetry log from file then launch IPython shell
#
# author: Ryan Kingsbury
#
# This script is used to inspect the contents of the specified log
#
# For command line help:
#     python inspect_log.py --help
#
####################################################################

# External libraries
import struct

# Custom libraries/modules
import pktlog
import pktparser
import crc16pure as crc16

def load_binary_log(input_filename, use_enum=False):
    """ Load binary log from file, return as dictionary object
    """
    
    # Create dictionary where we can store all parsed packets
    pdb = {}

    parser = pktparser.Parser()

    cnt = 0
    
    with open(input_filename,"r") as fin:
        for line in fin:
            try:
                (time,pktraw,framingType) = pktlog.read_entry(line)
            except TypeError:
                # Incomplete line (often caused by log file in progress)
                # Skip line and continue
                continue

            if framingType != "OBC":
                continue

            cnt += 1

            # Check the packet CRC
            # First, extract CRC from packet
            crc_pac = struct.unpack_from("H",pktraw,6)[0]
                    
            # Extract packet payload
            rxcpy = pktraw[8:]
                  
            # Create a temp copy of rx buffer to add zeroes if odd
            if len(rxcpy) % 2 != 0:
                rxcpy = rxcpy + chr(0)
                    
            # Calculate CRC across packet
            crc_calc = crc16.crc16xmodem(rxcpy)
             
            pkt = parser.parse_packet(pktraw,use_enum=use_enum)
            
            if crc_pac != crc_calc:
                print "CRC failed on line %i, length=%i, type=%s"%(cnt,len(pktraw),pkt['name'])
                print "[",
                for b in pktraw:
                    print hex(ord(b)),
                print "]"
                    
            
            if pkt==None:
                # Parser failure
                print "Failed to parse line %i:"%cnt, line
                continue
            
            # Add packet to database
            pdb.setdefault(pkt['name'], []).append(pkt['values'])

    print "Read %i packets including:"%cnt

    for p in pdb:
        print " %i %s packets"%(len(pdb[p]),p)
    
    return pdb
    
if __name__ == '__main__':
    import argparse

    # Define parser for command line arguments
    ap = argparse.ArgumentParser(description='inspect_log.py')
    ap.add_argument("file", help='input file', nargs='+')
    ap.add_argument('-s','--shell',
                    action='store_true',
                    dest='regShell',
                    help='launch a regular python shell')
    ap.add_argument('-i','--ipython-shell',
                    action='store_true',
                    dest='iShell',
                    help='launch an IPython shell')
    ap.add_argument('-e','--use-enum',
                    action='store_true',
                    dest='useEnum',
                    help='observe enum mappings specified in packet definition')

    
    # Parse the command line arguments
    args = ap.parse_args()

    for f in args.file:
        print "Processing",f
        tlm = load_binary_log(f,use_enum=args.useEnum)

        # Launch shell (either regular python, or IPython)
        if args.regShell:
            import pdb; pdb.set_trace()
        elif args.iShell:
            from IPython import embed
            embed()

        print



