#!/usr/bin/env python
import serial
import sys, time
from daemon import Daemon
from rotfile import RotatingFile

LOG_PATH = "/tmp"
SERIAL_PORT = "/dev/ttyUSB0"

class SerialLoggingDaemon(Daemon):
	def run(self):

                # Open USB-to-serial adapter
                try:
                        print "Trying to open",SERIAL_PORT
                        ser = serial.Serial(SERIAL_PORT,115200,timeout=0.1)
                except:
                        print "Failed to open",SERIAL_PORT

                logfile = RotatingFile(path=LOG_PATH)

                while True:
                        x = ser.read(1024)
                        print "Received %i bytes"%len(x)
                        logfile.write(x)

if __name__ == "__main__":
	daemon = SerialLoggingDaemon('/tmp/daemon-example.pid',stdout='/tmp/daemon-stdout')
	if len(sys.argv) == 2:
		if 'start' == sys.argv[1]:
			daemon.start()
		elif 'stop' == sys.argv[1]:
			daemon.stop()
		elif 'restart' == sys.argv[1]:
			daemon.restart()
		else:
			print "Unknown command"
			sys.exit(2)
		sys.exit(0)
	else:
		print "usage: %s start|stop|restart" % sys.argv[0]
		sys.exit(2)
