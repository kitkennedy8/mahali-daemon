####################################################################
# Binary Packet Parser
#
# author: Ryan Kingsbury
#
# This module parses binary packets into Python structures based on
# the specified packet formats.  Packet formats are specified in
# "schema" files stored in ./packet_defs/.
#
####################################################################
import glob
import os
import json
import struct

import headers
import mapping

PKT_HEADER_LENGTH = 8
DEBUG = False

# Get data type mappings
import mapping
DATA_TYPE_FMT = mapping.get_mappings()

def convert(input):
    """ Strip specified object of unicode strings. """
    if isinstance(input, dict):
        return {convert(key): convert(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

class Parser:
    
    def __init__(self,schema_path="packet_defs"):

        # Schema dictionary, indexed by packet ID
        self.schema = {}

        # Load all schema files in directory
        for fname in glob.iglob(os.path.join(schema_path,'*.json')):
            try: 
                self.load_schema(fname)
            except:
                print "\n\n*****************************************************"
                print "Error parsing packet definition file: %s"%fname
                print "*****************************************************"
                raise
                
        # TODO: Add statistics gathering to parser
            
    def load_schema(self,fname):
        """ Loads schema from specified filename and adds it to the
        schema dictionary. """

        with open(fname,'r') as f:
            if DEBUG: print "loading", fname
            # Load JSON file from disk and strip unicode formatting
            j = convert(json.load(f))
            self.schema[j['id']] = j
        

    def parse_packet(self, packet, use_enum=True):

        # Grab the packet ID
        pkt_id = struct.unpack_from("B",packet,1)[0]
        if DEBUG: print "packet ID", pkt_id

        # Confirm that packet ID is in the schema dictionary
        if pkt_id not in self.schema.keys():
            if DEBUG: print "ERROR: packet ID not recognized"
            return

        # Grab a copy of the schema for this packet.  We are going to
        # populate each of the field entries with a value.
        s = self.schema[pkt_id].copy()

        # Calculate the length field based on the fields specified in the schema
        s['length']=headers.calc_pkt_size(s)
        
        # Check if packet has a variable length name and override length if appropriate
        varlen_name = None
        if 'varlen' in s:
            varlen_name = s['varlen']
            if s['length']+PKT_HEADER_LENGTH <= len(packet):
                s['length'] = len(packet) - PKT_HEADER_LENGTH
        
        # Validate length of packet:
        #  We need to make sure that the packet length matches what is
        #  expected for packets with this ID.
        # Calculate length of packet
        calced_length = len(packet)-PKT_HEADER_LENGTH
        # Correct for sequence ID if it is present
        if "hasSequence" in s: calced_length -= 2
        if calced_length != s['length']:
            print "ERROR: unexpected packet length (was %i,expected %i)"%(calced_length,s['length']+PKT_HEADER_LENGTH)
            return

        # Index for tracking the parser's progress, we start with the
        # first byte after the packet header.
        index = PKT_HEADER_LENGTH

        # Create a dictionary to hold the parsed values
        # also create a dictionary for formatted values
        s['values'] = {}
        s['values_formatted'] = {}

        # If this packet type has a sequnece ID, then we want to start
        # field parsing two bytes later in the packet.
        if "hasSequence" in s:
            s['values']['seqID'] = struct.unpack_from("<H",packet,offset=index)
            index += 2
        
        # Parse fields in packet
        for f in s['fields']:
            
            # Look up the appropriate format description string
            fmt_string = DATA_TYPE_FMT[f['type']]

            # See if this field is an array
            array_len=1
            if 'array' in f:
                try:
                    array_len = int(f['array'])
                    fmt_string = fmt_string*array_len
                except ValueError:
                    print "Array parameter for field %s is not an integer."

            #  We add the '<' to specify little-endian
            fmt_string = "<" + fmt_string
            
            # Parse the valuse and store it
            if array_len==1:
                # if it is a single value, store just the value
                v = struct.unpack_from(fmt_string,packet,offset=index)[0]

                # if a formatting function (e.g. calibration) is specified, then apply it
                if "format_func" in f:
                    v_formatted = eval(f['format_func'])(v)
                    s['values_formatted'][f['name']] = v_formatted
                    
                # If this is an "enum" field then resolve the name
                if ("enum" in f) and use_enum:
                    try:
                        v = f['enum'][str(v)]
                    except KeyError:
                        pass
                s['values'][f['name']] = v
            else:
                # If it is a field, then unpack_from is going to return a list
                v_list = struct.unpack_from(fmt_string,packet,offset=index)

                # if a formatting function (e.g. calibration) is specified, then apply it
                if "format_func" in f:
                    v_list_formatted = []
                    for v in v_list:
                        v_list_formatted.append(eval(f['format_func'])(v))
                    s['values_formatted'][f['name']] = v_list_formatted

                # Deal with enumerated fields
                if ("enum" in f) and use_enum:
                    v_list_enumed = []
                    for v in v_list:
                        try:
                            v_list_enumed.append(f['enum'][str(v)])
                        except KeyError:
                            v_list_enumed.append(v)
                    s['values'][f['name']] = v_list_enumed
                else:
                    s['values'][f['name']] = list(v_list)
                
            # Calculate the next offset and add it to the index
            index += struct.calcsize(fmt_string)
            
        # Add the variable-length field to the packet    
        if varlen_name != None:
            s['values'][varlen_name] = packet[index:]

        return s

    def pretty_print_packet(self,pkt):
        """ This function results the packet as string. """

        try:
            s = pkt['name'] + ' ['
        except (TypeError,KeyError):
            s= "UNKNOWN" + ' ['
            return s
            
        try:
            for f in pkt['fields']:
                fn = f['name']

                try:
                    s+= "%s=%s "%(fn, str(pkt['values'][fn]))
                except KeyError: # Catch cases where value does not exist
                    s+= "%s=DNE! "%(fn)
        except KeyError:
            s+="ERROR: packet is lacking 'fields' key"

        s += "]"


        return s
