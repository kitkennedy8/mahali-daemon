"""
Simple Rotating File Implementation

R. Kingsbury

This class provides a file-like object that automatically rotates at
the top of every hour. 

"""
import datetime
import os

DEBUG=False

class RotatingFile(object):
    """ Provides a file-like object that automatically rotates at top
    of every hour.
    """
    def __init__(self,path='/tmp/TEST',starttime=None):
        self.logpath = path
        
        # Initial logfile takes on the current time of day, subsequent
        # logfiles (post-rotation) will be named with top of hour.
        if starttime:
            self.logtime = starttime
        else:
            self.logtime = datetime.datetime.utcnow()

        # Open initial logfile
        self.cur_logfile = self.get_filename(self.logtime)        
        self.fd = open(self.cur_logfile,'w')
        if DEBUG: print "Opening log",self.cur_logfile
        
    def get_filename(self,dt=None):
        """Returns full logfile path"""
        return self.logpath+dt.strftime("%Y%m%d_%H%M%S") 
        
    def write(self,data):
        """ Write data to file, checking for rotation first. """
        
        # If cur logfile time is > current file time, then rotate
        temp_logtime = self.get_cur_logtime()
        if temp_logtime>self.logtime:
            # Need to rotate
            self.rotate(temp_logtime)
        
        self.fd.write(data)
        self.fd.flush()
        
    def rotate(self,logtime):
        """Forces file rotation"""
        self.fd.close()

        self.logtime = logtime
        
        # Get new filename
        self.cur_logfile = self.get_filename(self.logtime)
        self.fd = open(self.cur_logfile,'w') 
        if DEBUG: print "Opening log",self.cur_logfile
        
    
    def get_cur_logtime(self):
        """ Returns a datetime object for the top of the current hour. """
        now = datetime.datetime.utcnow()
        return now.replace(minute=0,
                           second=0,
                           microsecond=0)

    def close(self):
        self.fd.close()
        
